document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {

    cordova.plugins.backgroundMode.on('activate', function() {
        cordova.plugins.backgroundMode.disableWebViewOptimizations();
    });

    cordova.plugins.backgroundMode.setDefaults({
      title:"Scramble",
      text:"Running in background",
      subText: "",
      icon: "statusicon",
      showWhen: false
    })
    cordova.plugins.backgroundMode.enable();

    document.addEventListener("online", function(){
      console.log('cordova reconnect')
      libwip2p.Peers.getActive().consecutiveFails = 0;
      if (libwip2p.Peers.getConnState() == 0) {
        libwip2p.Peers.connect();
      }
    }, false);
}

function cordovaShowNotification(from) {
  cordova.plugins.notification.local.schedule({
    title: 'Scramble',
    text: 'New message from ' + from,
    foreground: true,
    priority: 2,
    vibrate: true,
    smallIcon: 'res://statusicon',
    icon: 'res://statusicon'
  });
}
