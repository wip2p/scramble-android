window["libwip2p"] =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var ethers = __webpack_require__(2)
var Peers = __webpack_require__(1)

var wallet = null;
var accountChangeHandlers = [];
var bUseLocalStorage = false;

var useLocalStorage = function(mode) {
  bUseLocalStorage = mode;
}

var initWallet = function(callback) {

  if (!bUseLocalStorage) {
    wallet = ethers.Wallet.createRandom();
  } else {
    var storedWallet = localStorage.getItem('wip2p_wallet');
    if (storedWallet != null) {
      if (storedWallet.startsWith('0x')) {
        var keyBuffer = Buffer.from(storedWallet.substring(2), 'hex');
        wallet = new ethers.Wallet(keyBuffer);
      } else {
        wallet = ethers.Wallet.fromMnemonic(storedWallet);
      }
    } else {
        wallet = ethers.Wallet.createRandom();
        localStorage.setItem('wip2p_wallet', wallet.mnemonic.phrase);
    }
  }
}

var restoreWallet = function(seed, legacyAccount) {
    var privKey;

    if (legacyAccount == null) {
      legacyAccount = false;
    }

    if (legacyAccount) {
      // restore legacy account
      var privKey;
      try {
        var entropy = ethers.utils.mnemonicToEntropy(seed);
        wallet = new ethers.Wallet(entropy);
        privKey = wallet.privateKey;
      } catch (e) {
        console.error(e.message);
        return false;
      }
      if (bUseLocalStorage) {
        localStorage.setItem('wip2p_wallet', privKey);
      }
    } else {
      // restore account
      try {
        wallet = ethers.Wallet.fromMnemonic(seed);
      } catch (e) {
        console.error(e.message);
        return false;
      }
      if (bUseLocalStorage) {
        localStorage.setItem('wip2p_wallet', seed);
      }
    }

    for (var a = 0; a < accountChangeHandlers.length; a++) {
        if (accountChangeHandlers[a] != null)
            accountChangeHandlers[a]();
    }

    return true;
}

var newWallet = function() {
  return Peers.getActivePeerSession()
  .then((session)=>{
    return session.disconnect()
  })
  .then(()=>{
    wallet = ethers.Wallet.createRandom();
    localStorage.setItem('wip2p_wallet', wallet.mnemonic.phrase);
    for (var a = 0; a < accountChangeHandlers.length; a++) {
        if (accountChangeHandlers[a] != null)
            accountChangeHandlers[a]();
    }

    libwip2p.Peers.connect();
  })

  //console.log(wallet.mnemonic);

  // shorten the private key to 16 bytes (12 words)
  //var privKey = wallet.privateKey.substring(0,34);
  //wallet = new ethers.Wallet(privKey);

  //localStorage.setItem('wip2p_wallet', privKey);
}

var getWallet = function() {
    return wallet;
}

var sign = function(timestamp, multihash) {
    if (typeof multihash != 'string' || multihash.startsWith('0x' == false)) {
        console.error("Multihash doesn't appear to be hex string");
        return null;
    }

    var objToSign = [timestamp, multihash];
    var stringToSign = JSON.stringify(objToSign);

    return wallet.signMessage(stringToSign);
}

var subscribeAccountChange = function(callback) {
    //console.log('new subscriber')
    var bFound = false;
    var idx;
    for (var a = 0; a < accountChangeHandlers.length; a++) {
        if (accountChangeHandlers[a] == null) {
            idx = a;
            bFound = true;
            break;
        }
    }
    if (bFound)
        accountChangeHandlers[a] = callback;
    else
        var idx = accountChangeHandlers.push(callback) - 1;

    return idx;
}

var unsubscribeAccountChange = function(idx) {
    //console.log('unsubscribed')
    accountChangeHandlers[idx] = null;
}

var fetchDetails = function(account, options) {
  if (typeof options != 'object') {
    options = {}
  }
  if (options.hasOwnProperty('includePaste') == false) {
    options.includePaste = true;
  }

  return Peers.getActivePeerSession()
  .then((session)=>{
    return session.sendMessage({
      method: "ui_getAccount",
      params: [account, options]
    })
  })
  .then(function(response){

      if (response.error) {
          if (response.error.message)
            throw new Error(response.error.message);
          else
            throw new Error(response.error);
      }

      return response.result;
  })
  .catch(function(err) {
    //console.log(err)
    throw err;
  })
}

module.exports = {
    useLocalStorage: useLocalStorage,
    initWallet: initWallet,
    restoreWallet: restoreWallet,
    newWallet: newWallet,
    sign: sign,
    getWallet: getWallet,
    subscribeAccountChange: subscribeAccountChange,
    unsubscribeAccountChange: unsubscribeAccountChange,
    fetchDetails: fetchDetails
}


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

var Session = __webpack_require__(3)
var Peer = __webpack_require__(11)
const EventEmitter = __webpack_require__(4);

var peers = [];
var activePeerIdx = -1;
var activePeerSession = null;
var events = new EventEmitter();
var bUseLocalStorage = false;

var useLocalStorage = function(mode) {
  bUseLocalStorage = mode;
}

var init = async function(defaultPeers) {

  if (bUseLocalStorage) {
    var storedPeers = localStorage.getItem('peers');
    if (storedPeers != null) {
        var tmpPeers = JSON.parse(storedPeers);

        if (Array.isArray(tmpPeers) == false) {
            // its not an array so just use the default empty array for peers
        }
        else {
            for (var a = 0; a < tmpPeers.length; a++) {
              await Peer.load(tmpPeers[a])
              .then((peer)=>{
                //console.log(peer)
                peers.push(peer);
              })
              .catch((err)=>{})
            }
        }
    }
  }

  // always add default peers
  if (Array.isArray(defaultPeers)) {
    for (var a = 0; a < defaultPeers.length; a++) {
      await addPeer(defaultPeers[a]);
    }
  }

  //await addPeer("wss://wip2p.com/wip2p");

  var storedActivePeer;
  if (bUseLocalStorage) {
    storedActivePeer = localStorage.getItem('active_peer');
    if (storedActivePeer != null) {
      activePeerIdx = parseInt(storedActivePeer);
      if (activePeerIdx == 'NaN' || activePeerIdx >= peers.length || activePeerIdx < 0) {
        console.log('problem with active peer, resetting');
      }
    }
    if (peers.length > 0 && activePeerIdx == -1) {
      activePeerIdx = 0;
    }
  }

  if (bUseLocalStorage) {
    savePeersToStorage();
  }

  connect();
}

var connect = function() {
  var activePeer = getActive();
  if (activePeer == null) {
    return;
  }

  activePeer.lastAttempt = Math.round((new Date()).getTime() / 1000);

  return Session.create(activePeer)
  .then((session)=>{
    activePeerSession = session;
    activePeerSession.onBundlePublished = function() {
      events.emit('bundlepublished');
    }
    activePeerSession.onBundleReceived = function(bundle) {
      events.emit('bundlereceived', bundle);
    }
    activePeerSession.onConnStateChange = function(state, manualDisconnect) {
      events.emit('connstatechange', state, manualDisconnect);
    }
    activePeerSession.onInfo = function() {
      if (bUseLocalStorage) {
        savePeersToStorage();
      }
    }
    activePeerSession.onPeerIdChanged = function(endpoint, origPeerId, newPeerId) {
      events.emit('peeridchanged', endpoint, origPeerId, newPeerId);
    }
    activePeerSession.onPeerSwap = function(endpoints) {
      for (var a = 0; a < endpoints.length; a++) {
        addPeer(endpoints[a]);
      }
    }
    return activePeerSession.start()
  })
  .then(()=>{
    activePeer.consecutiveFails = 0;
    events.emit('peerconnected');
  })
  .catch((err)=>{
    activePeer.consecutiveFails++;
    //throw err;
  })
}

var getActivePeerSession = function() {
  return new Promise((resolve, reject)=>{
    if (activePeerSession == null)
      reject('peerSession not ready')
    else
      resolve(activePeerSession)
  })
}

var getConnState = function() {
  if (activePeerSession == null) {
    return 0;
  } else {
    return activePeerSession.connState;
  }
}

var addPeer = function(endpoint, peerId) {
  return new Promise((resolve, reject)=>{
    if (endpoint.startsWith("ws://") == false && endpoint.startsWith("wss://") == false) {
      return reject("peer expects ws:// or wss://");
    }
    var bFound = false;
    for (var a = 0; a < peers.length; a++) {
      if (peers[a].preferredEndpoint == endpoint) {
        bFound = true;
        resolve(peers[a]);
        break;
      }
      if (peerId != null && peers[a].peerId == peerId) {
        bFound = true;
        resolve(peers[a]);
        break;
      }
    }
    if (!bFound) {
      return Peer.create(endpoint, peerId)
      .then((p)=>{
        peers.push(p);
        if (activePeerIdx == - 1) {
          activePeerIdx = 0;
        }
        if (bUseLocalStorage) {
          savePeersToStorage();
        }
        if (peers.length == 1) {
          connect();
        }
        resolve(p);
      })
    }
  })
}

var savePeersToStorage = function() {
    localStorage.setItem('peers', JSON.stringify(peers));
    localStorage.setItem('active_peer', activePeerIdx);
}

var getPeers = function() {
    if (peers.length == 0)
      return [];

    var peersCopy = JSON.parse(JSON.stringify(peers));
    peersCopy[activePeerIdx].active = true;
    return peersCopy;
}

var getActive = function() {
  if (activePeerIdx == -1)
    return null;
  else
    return peers[activePeerIdx];
}

var updatePeerId = function(endpoint, newPeerId) {
  for (var a = 0; a < peers.length; a++) {
    if (peers[a].preferredEndpoint == endpoint) {
      peers[a].updatePeerIdOnly(newPeerId);
      savePeersToStorage();
      break;
    }
  }
}

var getActiveAddress = function() {
    return peers[activePeerIdx].address;
}

var setActive = function(idx) {
    if (peers.length <= idx) throw new Error("Unknown peer index");
    activePeerIdx = idx;

    if (bUseLocalStorage) {
      localStorage.setItem('active_peer', activePeerIdx);
    }

    // close the existing socket
    activePeerSession.disconnect();

    connect();
}

var remove = function(peerIdx) {

    if (peerIdx == activePeerIdx && peers.length == 1) {
      activePeerIdx = -1;
      localStorage.setItem('active_peer', activePeerIdx);
    } else if (peerIdx <= activePeerIdx) {
      activePeerIdx--;
      localStorage.setItem('active_peer', activePeerIdx);
    }

    peers.splice(peerIdx, 1);
    localStorage.setItem('peers', JSON.stringify(peers));
}

var removeByEndpoint = function(endpoint) {
  for (var a = 0; a < peers.length; a++) {
    if (peers[a].preferredEndpoint == endpoint) {
      this.remove(a);
      break;
    }
  }
}

var block = function(peerIdx) {
    peers[peerIdx].block = true;
    if (bUseLocalStorage) {
      localStorage.setItem('peers', JSON.stringify(peers));
    }
}

var unblock = function(peerIdx) {
    peers[peerIdx].block = false;
    if (bUseLocalStorage) {
      localStorage.setItem('peers', JSON.stringify(peers));
    }
}

module.exports = {
    useLocalStorage: useLocalStorage,
    init: init,
    getPeers: getPeers,
    getActive: getActive,
    setActive: setActive,
    getActiveAddress: getActiveAddress,
    addPeer: addPeer,
    remove: remove,
    removeByEndpoint: removeByEndpoint,
    updatePeerId: updatePeerId,
    block: block,
    unblock: unblock,
    getActivePeerSession: getActivePeerSession,
    getConnState: getConnState,
    connect: connect,
    events: events
}


/***/ }),
/* 2 */
/***/ (function(module, exports) {

(function() { module.exports = window["ethers"]; }());

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var nacl = __webpack_require__(10)
var ethers = __webpack_require__(2)

var create = function(peer) {

  return new Promise(function(resolve, reject){

    if (peer == null || peer.constructor.name != 'Peer') {
      return reject ('expects object type Peer');
    }

    var ps = new PeerSession();

    ps.peer = peer;

    resolve(ps)
  })
}

var PeerSession = function() {
  this.requestCounter = 0;
  this.pendingRequests = {};
  this.websocket = null;

  this.peer = null;

  // 0 disconnected, 1 connecting, 2 connection established, 3 not authorized, 4 auth succeeded, 5 session already active
  // auth failure or peerId change results in immediate disconnect
  this.connState = 0;
  this.connLastError = null;
  this.remoteAuthPubKey = null;
  this.localAuthKeyPair = nacl.box.keyPair();
  this.publicKeysExchanged = false;
  this.remotePeerId = null;

  this.onConnStateChange = null;
  this.onInfo = null;
  this.onBundlePublished = null;
  this.onBundleReceived = null;
  this.onPeerIdChanged = null;
  this.onPeerSwap = null;
  this.asasdasd = 1

  Object.seal(this);
}

PeerSession.prototype.start = function() {
  var self = this;

  return self.connect()
  .then(()=>{
    return self.doPeerHello();
  })
  .then(()=>{
    return self.doPeerAuth();
  })
  .then(()=>{
    self.updateConnState(4);
    if (typeof self.onAuthed == 'function') {
      self.onAuthed();
    }
    // do PeerInfo()
    return self.doPeerInfo();
  })
  .then(()=>{
    if (typeof self.onInfo == 'function') {
      self.onInfo();
    }
    //console.log(self.peer);
  })
  .then(()=>{
    return self.doPeerSwap();
  })
  .catch((err)=>{
    if (err == "peer did not match expected") {
      //console.log('firing peerid change event if its registered')
      if (typeof self.onPeerIdChanged == 'function') {
        self.connLastError = "peerid has changed";
        self.onPeerIdChanged(self.peer.preferredEndpoint, self.peer.peerId, self.remotePeerId);
      }
    } else if (err == "account not authorized" || err == "unauthorized") {
      self.updateConnState(3);
    } else if (err == "peer already has another session") {
      self.updateConnState(5);
    } else {
      console.log(err);
    };
    throw err;
  })
}

PeerSession.prototype.updateConnState = function(newState, manualDisconnect) {
  this.connState = newState;
  if (typeof this.onConnStateChange == 'function')
    this.onConnStateChange(newState, manualDisconnect);
}

PeerSession.prototype.Decrypt = function(encryptedBytes, outDecrypted) {

  var self = this;

  var nonce = new Uint8Array(encryptedBytes.slice(0, 24));
  var msg = new Uint8Array(encryptedBytes.slice(24));

  var theirPubKey = ethers.utils.arrayify(self.remoteAuthPubKey);
  var myPrivKey = ethers.utils.arrayify(self.localAuthKeyPair.secretKey);

  return nacl.box.open(msg, nonce, theirPubKey, myPrivKey)
}

PeerSession.prototype.Encrypt = function(unencryptedString) {
  var self = this;
  // perform asymmetric key exchange local private key & remote pub key
  var nonce = nacl.randomBytes(24);
  //console.log(nonce)
  var theirPubKey = ethers.utils.arrayify(self.remoteAuthPubKey);
  //console.log(theirPubKey)
  var myPrivKey = ethers.utils.arrayify(self.localAuthKeyPair.secretKey);
  //console.log(myPrivKey)

  var msg = new TextEncoder("utf-8").encode(unencryptedString);
  var enc = nacl.box(msg, nonce, theirPubKey, myPrivKey)
  //console.log(enc)

  var fullmsg = new Uint8Array(enc.length + 24);
  fullmsg.set(nonce)
  fullmsg.set(enc, nonce.length)
  //console.log(fullmsg)

  return fullmsg;
}

PeerSession.prototype.getNextRequestId = function() {
  this.requestCounter++;
  return this.requestCounter;
}

PeerSession.prototype.connect = function() {
  var self = this;
  return new Promise(function(resolve, reject){

    if (self.peer.preferredEndpoint == null) {
      return reject('peer missing endpoint');
    }

    var activePeerAddress = self.peer.preferredEndpoint;
    if (activePeerAddress.startsWith('http://')) {
      activePeerAddress = 'ws://' + activePeerAddress.substr(7);
    } else if (activePeerAddress.startsWith('https://')) {
      activePeerAddress = 'wss://' + activePeerAddress.substr(8);
    }

    try {
      self.websocket = new WebSocket(activePeerAddress);
      self.updateConnState(1);
    } catch(err) {
      return reject(err);
    }

    self.websocket.onopen = function(){
      self.updateConnState(2);
      resolve();
    }

    self.websocket.onmessage = function(message){

      new Promise(function(resolve, reject){
        var reader = new FileReader()
        reader.onload = function(){
          resolve(reader.result)
        }
        reader.readAsArrayBuffer(message.data)
      })
      .then(function(messageDataBuffer){
        if (self.publicKeysExchanged) {
          var data = self.Decrypt(messageDataBuffer);
          if (data != null) {
            return new TextDecoder().decode(data);
          } else {
            console.error("Problem decrypting incoming message...")
            return new TextDecoder().decode(messageDataBuffer);
          }
        } else {
          return new TextDecoder().decode(messageDataBuffer);
        }
      })
      .then(function(messageData){
        if (window.logWebsocket)
          console.log('%c[websocket] <- ' + messageData, "color: green;")

        var parsedMessageData = JSON.parse(messageData);
        if (parsedMessageData.hasOwnProperty('id') == false && parsedMessageData.error) {
          console.error("remote peer responded with error:")
          throw new Error(parsedMessageData.error);
        } else {
          return parsedMessageData;
        }
      })
      .then(function(messageData){
        //todo handle incoming requests (i.e new pastes)
        if (messageData.method) {
          if (messageData.method == 'bundle_save') {
            if (typeof self.onBundleReceived == 'function')
              self.onBundleReceived(messageData.params[0])
          } else {
            console.log(messageData);
          }

          // response with ok no matter what
          self.sendMessage({id: messageData.id, result: 'ok'});

          return;
        }

        if (self.pendingRequests[messageData.id]) {
          if (messageData.hasOwnProperty('error')) {
            self.pendingRequests[messageData.id].reject(messageData.error);
          } else {
            self.pendingRequests[messageData.id].resolve(messageData);
          }
          delete self.pendingRequests[messageData.id];
        } else {
          console.log('got a response from an unknown request');
        }
      })
      .catch((err)=>{
        if (err.message == "account not authorized") {
          console.log(err)
        }
        console.error(err);
      })
    }

    self.websocket.onerror = function(event) {
      return reject('error with websocket');
    }

    self.websocket.onclose = function(event) {
      console.error('connection to peer closed')

      for (var key in self.pendingRequests) {
        self.pendingRequests[key].reject("peer disconnected");
      }

      let manualDisconnect = false;
      self.updateConnState(0, manualDisconnect);
    }

  })
}

PeerSession.prototype.disconnect = function() {
  this.websocket.onclose = null;
  this.websocket.close();
  let manualDisconnect = true;
  this.updateConnState(0, manualDisconnect);
}

PeerSession.prototype.sendMessage = function(messageData) {

  var self = this;

  return new Promise(function(resolve, reject){

    if (typeof messageData != 'object') {
      return reject('messageData not object');
    }

    if (messageData.method != null) {
      // only allow these two requests if handshake is not complete
      if (self.handshakeComplete == false && (
        messageData.method != 'peer_hello' &&
        messageData.method != 'peer_auth' &&
        messageData.method != 'ui_requestInvite')
      ) {
        return reject('peerSession not ready');
      }

      var id = self.getNextRequestId();
      messageData.id = id;

      if (messageData.params == null)
      messageData.params = [];

    } else if (messageData.result != null) {}
    else {
      return reject("messageData has neither method or result!");
    }

    messageData = JSON.stringify(messageData);

    var doSend = function() {

      if (window.logWebsocket)
        console.log('%c[websocket] -> ' + messageData, "color: green;")

      var msgData = Buffer.from(messageData)
      if (self.publicKeysExchanged) {
        msgData = self.Encrypt(msgData);
      }
      self.websocket.send(msgData);
      self.pendingRequests[id] = {resolve: resolve, reject: reject};
    }

    // add to callback queue
    if (self.websocket == null || self.websocket.readyState != 1) {
      return reject("websocket not connected unable to sendMessage");
    } else {
      doSend();
    }
  })
}

PeerSession.prototype.doPeerSwap = function() {
  var params = [];
  var self = this;

  // if we are hosted on https then request only secure peers
  if (window.location.protocol == 'https:')
    params = [ {secureOnly: true} ];

  this.sendMessage({method:"peer_swap", params: params})
  .then(function(response){
    if (response.result.hasOwnProperty('endpoints') == false) {
      return;
    }
    self.onPeerSwap(response.result.endpoints)
  })
}

PeerSession.prototype.doPeerHello = function() {
  var self = this;

  return new Promise(function(resolve, reject){

    var pubKey = ethers.utils.hexlify(self.localAuthKeyPair.publicKey);

    return self.sendMessage({method:"peer_hello", params: [{pubKeyAuth: pubKey}]})
    .then((response)=>{
      if (response.result.pubKeyAuth.length > 66)
        return reject("invalid public key from remote");

      self.remoteAuthPubKey = response.result.pubKeyAuth;
      self.publicKeysExchanged = true;
      resolve();
    });

  });
}

PeerSession.prototype.doPeerAuth = function() {
  var self = this;

  return new Promise(function(resolve, reject){

    // sign our encryption pubkey
    var pubKeyString = ethers.utils.hexlify(self.localAuthKeyPair.publicKey)
    __webpack_require__(0).getWallet().signMessage(pubKeyString)
    .then((sig)=>{
      return self.sendMessage({method:"peer_auth", params: [{
          signature: sig
        }]
      });
    })
    .then((response)=>{

      if (response.error) {
        return reject(response.error);
      }

      var derivedAccountString;
      try {
        var signature = response.result.signature;
        var stringToSign = self.remoteAuthPubKey;
        var hash = ethers.utils.hashMessage(stringToSign);
        derivedAccountString = ethers.utils.recoverAddress(hash, signature);
      } catch(err) {
        return reject(err);
      }

      self.remotePeerId = derivedAccountString;

      return self.peer.updatePeerId(self.remotePeerId)
      .then(()=>{
        //self.connReady = true;
        resolve();
      })
      .catch((err)=>{
        console.log(err)
        self.websocket.close();
        return reject("peer did not match expected");
      })
    })
    .catch((err)=>{
      reject(err);
    })
  });
}

PeerSession.prototype.doBundlePublish = function(bundle) {
  var self = this;
  return this.sendMessage({method:"paste_save", params: [ bundle ]})
  .then((result)=>{
    if (typeof self.onBundlePublished == 'function')
      self.onBundlePublished();
      return result;
  })
}

PeerSession.prototype.doPeerInfo = function(){
  var self = this;
  return libwip2p.Peers.getActivePeerSession()
  .then((session)=>{
    return session.sendMessage({method: "peer_info"});
  })
  .then((response)=>{
    if (response.error) {
      return reject(response.error);
    }

    // maybe verify root accounts? make sure we talking the same tree
    self.peer.updateInfo(response.result);
  })
}

module.exports = {
  create: create
}


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

function checkListener(listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
}

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function _getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  checkListener(listener);

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0)
      return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      checkListener(listener);
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      checkListener(listener);

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var session = __webpack_require__(3);
var account = __webpack_require__(0);

var cachedDocs = [];

// path may not exist in found content
// requesting content encounters error
// requested content may not exist on peer
// ? should we return the desired object from path or the full cid object at path?

var getContentByKey = function(key) {
  console.log('deprecated, use BranchSet instead')

  return new Promise((resolve, reject)=>{
    if (typeof key != 'string' || key.length == 0) {
      return reject("invalid key");
    }

    var curAccount = account.getWallet().address;
    account.fetchDetails(curAccount)
    .then((result)=>{
      var bCbor = Buffer.from(result.cborData[0], 'base64');
      var doc = IpldCbor.util.deserialize(bCbor);
      decodeSpecials(doc);
      cachedDocs[0] = {key: "/", content: doc}
      var targetObj = walkCachedDocsByPath(key, doc);
      resolve(targetObj);
    })
    .catch((err)=>{
      reject(err);
    })

  })
}

var setContentByKey = function(path, value) {
  console.log('deprecated, use BranchSet instead')
  // get root content
  // add key / value
  //console.log('setContentByKey')

  if (cachedDocs.length == 0) {
    cachedDocs.push({key:"/", content: {}})
  }

  if (typeof cachedDocs[0].content != 'object') {
    cachedDocs[0].content = {};
  }

  var pathParts = path.split("/");
  pathParts.splice(0, 1);
  var key = pathParts[pathParts.length - 1];
  pathParts.splice(pathParts.length - 1, 1);
  var newPath = "/" + pathParts.join("/");

  var bCreateMissing = true;
  var targetObj = walkCachedDocsByPath(newPath, cachedDocs[0].content, bCreateMissing);
  targetObj[key] = value;
  //console.log(cachedDocs)
}

var updateHashes = function() {
  return new Promise((resolve, reject)=>{

    var encodedDoc = encodeSpecials(cachedDocs[0].content);
    var buf = IpldCbor.util.serialize(encodedDoc);
    cachedDocs[0].bytes = buf;

    IpldCbor.util.cid(cachedDocs[0].bytes)
    .then((cid)=>{
      cachedDocs[0].multihash = cid.multihash;
      resolve(cid.multihash);
    })
  })
}

var getDocDataArrayAsHex = function() {
  return [ cachedDocs[0].bytes.toString('hex') ]
}

var getDocDataArrayAsBase64 = function() {
  return [ cachedDocs[0].bytes.toString('base64') ]
}


var walkCachedDocsByPath = function(path, obj, bCreateMissing) {
  console.log('deprecated, use BranchSet instead')
  var pathParts = path.split("/");
  pathParts.splice(0,1);
  var tmpObj = obj;
  for (var idx = 0; idx < pathParts.length; idx++){
    if (tmpObj.hasOwnProperty(pathParts[idx])) {
      if (idx == pathParts.length - 1) {
        return tmpObj[pathParts[idx]];
      } else {
        tmpObj = tmpObj[pathParts[idx]];
      }
    } else {
      if (bCreateMissing) {
        tmpObj[pathParts[idx]] = {}
        return tmpObj[pathParts[idx]];
      } else {
        return null;
      }
    }
  }
}

var serializeToBuffer = function(stringPasteDoc) {
  return new Promise(function(resolve, reject) {
    var pasteDoc = {};

    var tmpDoc;
    try {
      tmpDoc = JSON.parse(stringPasteDoc);
    } catch (err) {
      tmpDoc = stringPasteDoc;
    }

    pasteDoc = tmpDoc;

    var encodedDoc = encodeSpecials(pasteDoc);
    var encodedCbor = libipfs.dagCbor.encode(encodedDoc);

    var cborData = Buffer.from(encodedCbor);
    resolve(cborData);
  })
}

var deserialize = function(byteData) {
    return new Promise(function(resolve, reject){
        var bCbor;
        if (Buffer.isBuffer(byteData))
            bCbor = byteData;
        else
            bCbor = Buffer.from(byteData.substr(2), 'hex');

        var pasteDoc = libipfs.dagCbor.decode(bCbor);
        decodeSpecials(pasteDoc);
        resolve(pasteDoc);
    })
}

var encodeSpecials = function(obj) {

    // if the data is a string
    if (typeof obj == 'string') {
        if (obj.startsWith('0x'))
            return Buffer.from(obj.substr(2), 'hex');
        else
            return obj;
    }

    if (typeof obj == 'number')
        return obj;

    if (Object.keys(obj).length == 1 && obj.hasOwnProperty("/")) {
        return new Cid(obj["/"]);
    }

    var newObj;
    if (Array.isArray(obj))
      newObj = []
    else
      newObj = {}

    for (var key in obj) {

        if (obj[key] == null) {
            newObj[key] = null;
        } else if (typeof obj[key] == 'object') {
            if (Object.keys(obj[key]).length == 1 && obj[key].hasOwnProperty('/')) {
                try {
                  var c = new Cid(obj[key]["/"]);
                  newObj[key] = c;
                } catch (err) {
                  console.log("appears to be an invalid CID");
                }
            } else {
                // recurse into child objects
                newObj[key] = encodeSpecials(obj[key]);
            }
        } else if (typeof obj[key] == 'string' && obj[key].startsWith('0x') == true) {
            var re = /^0x[\dA-F]+$/i;
            if (re.test(obj[key])) {
                // replace binary strings with Buffers
                try {
                  newObj[key] = Buffer.from(obj[key].substr(2), 'hex');
                } catch (e) {
                  newObj[key] = obj[key];
                }
            } else {
              newObj[key] = obj[key];
            }
        } else {
          newObj[key] = obj[key];
        }
    }

    return newObj;
}

var decodeSpecials = function(obj) {
    for (var key in obj) {
        if (obj[key] == null) { // do nothing
          obj[key] = null;
        } else if (obj[key].constructor.name == 'CID') {
            var cid = new Cid(obj[key]);
            obj[key] = {"/": cid.toString()}
        } else if (Buffer.isBuffer(obj[key])) {
            obj[key] = '0x' + obj[key].toString('hex');
        } else if (typeof obj[key] == 'object') {
            decodeSpecials(obj[key]);
        }
    }
}

var getCid = function(data) {
    return new Promise(function(resolve, reject) {
        var bCbor;
        if (Buffer.isBuffer(data) == false)
          bCbor = Buffer.from(data.substr(2), 'hex');
        else {
          bCbor = data;
        }

        sha256.digest(bCbor)
        .then((digest)=>{
          var cid = CID.create(1, dagCbor.code, digest)
          resolve(cid);
        })
    })
}

var CID = window.libipfs.multiformats.CID;
var dagCbor = window.libipfs.dagCbor;

const sha256 = window.libipfs.multiformats.hasher.from({
  name: 'sha2-256',
  code: 0x12,
  encode: (input) => libipfs.shajs('sha256').update(input).digest()
})

module.exports = {
  getContentByKey: getContentByKey,
  setContentByKey: setContentByKey,
  updateHashes: updateHashes,
  getDocDataArrayAsHex: getDocDataArrayAsHex,
  getDocDataArrayAsBase64: getDocDataArrayAsBase64,
  serializeToBuffer: serializeToBuffer,
  deserialize: deserialize,
  getCid: getCid
}


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


const Sigbundle = __webpack_require__(7)
const Account = __webpack_require__(0)
const Peers = __webpack_require__(1)
const Content = __webpack_require__(5);

var BranchSetNode = function() {
  this.content = null;
  this.docBytes = null;
  this.sizeBytes = null;
  this.multihash = null;
  this.hashVerified = null;
  this.isModified = null;
  this.links = [];

  return this;
}

BranchSetNode.prototype._setPathContent = function(path, content, bCreateMissing) {
  //console.log("_setPathContent")

  var self = this;
  return new Promise((resolve, reject)=>{
    var pathParts = path.split("/");
    pathParts.splice(0,1);

    if (self.content == null || typeof self.content == 'string')
      self.content = {};

    var tmpObj = self.content;
    for (var idx = 0; idx < pathParts.length; idx++){
      if (tmpObj.hasOwnProperty(pathParts[idx])) {
        if (idx == pathParts.length - 1) {
          tmpObj[pathParts[idx]] = content;
          return resolve();
        } else {
          tmpObj = tmpObj[pathParts[idx]];
        }
      } else {
        if (bCreateMissing) {
          if (idx == 0 && self.content == null) {
            self.content = {};
            tmpObj = self.content
          }
          if (idx == pathParts.length - 1) {
            tmpObj[pathParts[idx]] = content;
            return resolve();
          } else {
            tmpObj[pathParts[idx]] = {};
            tmpObj = tmpObj[pathParts[idx]];
          }
        } else {
          return reject('path doesnt exist');
        }
      }
    }
  })
}

var Links = function() {
  this.cid = null;
  this.sizeBytes = null;
}

var BranchSet = function() {
  this.nodes = []; // [FullPath, BranchSetNode]
  this.address = null;
  this.timestamp = null;
  this.signature = null;
  this.sigVerified = null;
  this.cummulativeSize = 0;
  this.hasFetched = false;

  this.onDocDownload = null;
  this.onDocVerified = null;
  this.onSigVerified = null;
}

BranchSet.prototype.FetchByAccount = function(account, path) {

  var self = this;

  this.sigVerified = false;
  this.path = path;

  return Peers.getActivePeerSession()
  .then((session)=>{
    return session.sendMessage({method:"bundle_get", params: [ {account: account} ]})
  })
  .then(async (response)=>{
    // fire off the download event
    if (typeof self.onDocDownload == 'function') {
      self.onDocDownload(0);
    }
    var bundle = response.result;
    this.signature = Buffer.from(bundle.signature.substr(2), 'hex');
    this.timestamp = bundle.timestamp;
    this.address = account;
    this.publicKey = Sigbundle.recoverPublicKey(bundle.timestamp, bundle.multihash, bundle.signature);

    // verify the signature and fire sig verify event
    if (Sigbundle.verifySignature(account, bundle.timestamp, bundle.multihash, bundle.signature) == false) {
      self.sigVerified = false;
      throw new Error('invalid signature');
    } else {
      self.sigVerified = true;
      if (typeof self.onSigVerified == 'function') {
        self.onSigVerified();
      }
    }

    if (Array.isArray(bundle.cborData) == false || bundle.cborData.length == 0 || bundle.cborData[0].length == 0) {
      throw new Error('no data returned');
    }

    // check content and fire event
    var docBytes = Buffer.from(bundle.cborData[0], 'base64');
    var mhash;
    await Content.getCid(docBytes)
    .then((_cid)=>{
      mhash = '0x' + Buffer.from(_cid.multihash.bytes).toString('hex');
    });
    if (mhash == bundle.multihash.toLowerCase()) {
      if (typeof self.onDocVerified == 'function') {
        self.onDocVerified(0);
      }
    } else {
      throw new Error('content does not match multihash');
    }
    mhash = null;

    // add the root node to the nodelist
    var node = new BranchSetNode()
    Object.seal(node);
    node.multihash = Buffer.from(bundle.multihash.substr(2), 'hex');
    node.hashVerified = true;
    node.sizeBytes = docBytes.length;
    await Content.deserialize(docBytes)
    .then((doc)=>{
      node.content = doc;
    })

    this.nodes.push(["/", node]);
    this.hasFetched = true;

    if (path == "/") {
      return node.content;
    } else {
      return self._traverseDocByPath(path)
      .catch((err)=>{
        this.error = err;
      })
    }
  })
}

/*BranchSet.prototype.FetchByMultihash = function(multihash, path) {
  return new Promise((resolve, reject)=>{reject("Not yet implemented")})
}

BranchSet.prototype.FetchByRootDoc = function(rootDoc, path) {
  return new Promise((resolve, reject)=>{reject("Not yet implemented")})
}*/

BranchSet.prototype.Update = function(path, content, createMissing) {
  var self = this;

  return new Promise(async (resolve, reject)=>{
    self.signature = null;
    var timestamp = Math.round((new Date()).getTime() / 1000);

    // ensure timestamp is newer
    if (timestamp <= self.timestamp) {
      return reject("Timestamp not newer than existing, check your computer clock.");
    }
    self.timestamp = timestamp;
    // currently hardcoded to rootNode
    if (self.nodes.length == 0) {
      self.nodes.push(["/", new BranchSetNode()])
    }
    var targetNode = self.nodes[self.nodes.length - 1][1];
    targetNode._setPathContent(path, content, createMissing);
    targetNode.isModified = true;

    await Content.serializeToBuffer(targetNode.content)
    .then((docBytes)=>{
      targetNode.docBytes = docBytes;
      targetNode.sizeBytes = docBytes.length;
      return Content.getCid(docBytes);
    })
    .then((cid)=>{
      targetNode.multihash = Buffer.from(cid.multihash.bytes).toString('hex');
    })

    resolve();
  })
}

BranchSet.prototype._traverseDocByPath = function(path, nodeIdx, bCreateMissing) {
  var self = this;
  return new Promise((resolve, reject)=>{
    if (self.nodes.length == 0)
      return reject('path doesnt exist')
    if (nodeIdx == null)
      nodeIdx = 0;
    var pathParts = path.split("/");
    pathParts.splice(0,1);
    var tmpObj = self.nodes[nodeIdx][1].content;
    for (var idx = 0; idx < pathParts.length; idx++){
      if (tmpObj.hasOwnProperty(pathParts[idx])) {
        if (idx == pathParts.length - 1) {
          return resolve(tmpObj[pathParts[idx]]);
        } else {
          tmpObj = tmpObj[pathParts[idx]];
        }
      } else {
        if (bCreateMissing) {
          tmpObj[pathParts[idx]] = {}
          return resolve(tmpObj[pathParts[idx]]);
        } else {
          // if we dont find the path then fail
          return reject('path doesnt exist')
        }
      }
    }
  })
}

BranchSet.prototype.sign = function() {
  var self = this;

  return Account.sign(self.timestamp, '0x' + self.nodes[0][1].multihash.toString('hex'))
  .then((signature)=>{
    self.signature = signature;
  })
}

BranchSet.prototype.exportSigBundle = function() {

  if (this.address == null) {
    throw 'no account set';
  }

  return {
    account: this.address,
    timestamp: this.timestamp,
    multihash: '0x' + this.nodes[0][1].multihash.toString('hex'),
    signature: this.signature,
    cborData: [ this.nodes[0][1].docBytes.toString('base64') ]
  }
}

BranchSet.prototype.exportRootCid = function() {
  return new Cid(1, 'dag-pb', this.nodes[0][1].multihash)
}

BranchSet.prototype.getTotalBytes = function() {
  // iterate through each branch doc
  //sum up all the links sizes
  var totalSize = 0;
  for (var a = 0; a < this.nodes.length; a++) {
    totalSize += this.nodes[a][1].sizeBytes;
  }
  return totalSize;
}

module.exports = BranchSet;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

var ethers = __webpack_require__(2)

var create = function(data){
  if (Buffer.isBuffer(data.multihash)) {
    data.multihash = '0x' + data.multihash.toString('hex')
  }

  if (Buffer.isBuffer(data.cborData[0])) {
    data.cborData[0] = '0x' + data.cborData[0].toString('hex')
  }

  return data;
}

var verifySignature = function(account, timestamp, multihash, signature) {

    var objToSign = [timestamp, multihash];
    var stringToSign = JSON.stringify(objToSign);
    var hash = ethers.utils.hashMessage(stringToSign);
    var derivedAccountString = ethers.utils.recoverAddress(hash, signature);

    return derivedAccountString.toLowerCase() == account.toLowerCase();
}

var recoverPublicKey = function(timestamp, multihash, signature) {
  var objToSign = [timestamp, multihash];
  var stringToSign = JSON.stringify(objToSign);
  var hash = ethers.utils.hashMessage(stringToSign);
  return ethers.utils.recoverPublicKey(hash, signature);
}

module.exports = {
  create: create,
  verifySignature: verifySignature,
  recoverPublicKey: recoverPublicKey
}


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

if (typeof window !== 'undefined') {
  if (typeof window.ethers == 'undefined') {
    throw new Error('missing dependency ethers')
  }
  if (typeof window.nacl == 'undefined') {
    throw new Error('missing dependency nacl')
  }
  if (typeof window.libipfs == 'undefined') {
    throw new Error('missing dependency libipfs')
  }
}

module.exports = __webpack_require__(9)


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

// stores
var account = __webpack_require__(0)
var peers = __webpack_require__(1)
var following = __webpack_require__(12)

var session = __webpack_require__(3)
var content = __webpack_require__(5)
var invites = __webpack_require__(13)
var sigbundle = __webpack_require__(7)
var branchset = __webpack_require__(6)

var useLocalStorage = function(mode) {
  account.useLocalStorage(mode);
  peers.useLocalStorage(mode);
  following.useLocalStorage(mode);
}

// default to not using localStorage, in memory only
useLocalStorage(false);

module.exports = {
  Account: account,
  Peers: peers,
  Following: following,
  Session: session,
  Content: content,
  Invites: invites,
  SigBundle: sigbundle,
  BranchSet: branchset,
  version: "0.2.3",
  useLocalStorage: useLocalStorage
}


/***/ }),
/* 10 */
/***/ (function(module, exports) {

(function() { module.exports = window["nacl"]; }());

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var create = function(endpoint, peerId) {

  return new Promise(function(resolve, reject){

    if (endpoint == null) {
      return reject ('endpoint is null');
    }

    var p = new Peer();

    Object.seal(p);

    // assign incoming data
    p.preferredEndpoint = endpoint;
    p.peerId = peerId;
    p.created = Math.round((new Date()).getTime() / 1000);

    if (endpoint.startsWith('ws://127.0.0.1') || endpoint.startsWith('wss://127.0.0.1')) {
      p.trusted = true;
    }

    resolve(p)
  })
}

var load = function(data) {
  return new Promise(function(resolve, reject){

    if (typeof data == 'string') {
      data = {preferredEndpoint: data}
    }

    var p = new Peer();
    Object.seal(p);

    // assign incoming data
    for (var key in data) {
      p[key] = data[key];
    }

    if (p.preferredEndpoint == null)
      return reject('missing preferredEndpoint');

    resolve(p);
  })
}


var Peer = function() {
  this.preferredEndpoint = null;
  this.peerId = null;

  this.version = null;
  this.merklehead = null;
  this.rootAccount = null;
  this.endpoints = [];
  this.sequenceSeed = null;
  this.latestSequenceNo = null;

  this.label = null;
  this.ignorePeerIdChanges = false;
  this.block = false;
  this.trusted = false;
  this.created = null;
  this.lastComms = null;
  this.lastAttempt = null;
  this.consecutiveFails = 0;
  this.lastError = null;
}

Peer.prototype.updatePeerId = function(remoteProvidedPeerId) {
  var self = this;
  return new Promise(function(resolve, reject){
    if (self.peerId == null || self.ignorePeerIdChanges || self.preferredEndpoint == "wss://ownpaste.com/api") {
      self.peerId = remoteProvidedPeerId;
    } else {
      if (self.peerId != remoteProvidedPeerId) {
        return reject("peerId doesn't match");
      }
    }
    self.lastComms = Math.round((new Date()).getTime() / 1000);
    self.consecutiveFails = 0;
    self.lastError = null;
    resolve();
  })
}

Peer.prototype.updateInfo = function(info) {
  this.rootAccount = info.rootAccount;
  this.version = info.version;
  this.merklehead = info.merklehead;
  this.sequenceSeed = info.sequenceSeed;
  this.latestSequenceNo = info.latestSequenceNo;
}

Peer.prototype.updatePeerIdOnly = function(remoteProvidedPeerId) {
  this.peerId = remoteProvidedPeerId;
}

module.exports = {
  create: create,
  load: load
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var peers = __webpack_require__(1)
var EventEmitter = __webpack_require__(4)

var follows = [];
var newPastes = {};
var events = new EventEmitter();
var bUseLocalStorage = false;

var useLocalStorage = function(mode) {
  bUseLocalStorage = mode;
}

var init = function() {
    var storedFollows = localStorage.getItem('following');
    if (storedFollows != null) {
        follows = JSON.parse(storedFollows);
    } else {
        follows.push({
            account: "0x8233afa23d338b51ba10b4c77d6bdcb0dad1490c",
            lastRead: 0
        });
        localStorage.setItem('following', JSON.stringify(follows));
    }
}

var checkAllForUpdates = function() {

  return new Promise(function(resolve, reject) {

    if (follows.length == 0)
        return;

    var accountsBatch = [];

    peers.getActivePeerSession()
    .then((activeSession)=>{
      if (activeSession == null)
        return;
      if (activeSession.connReady == false)
        return;

      for (var a = 0; a < follows.length && a < 10; a++) {
          accountsBatch.push(follows[a].account);
      }
      // do the request
      return activeSession.sendMessage({
        method: "ui_getTimestampsBatch",
        params: accountsBatch
      })
      .then(function(response){
        if (response.error) {
            reject(response.error);
            return;
        }

        var timestamps = response.result;
        if (Array.isArray(timestamps) == false)
          timestamps = [];
        var bFoundUnread = false;
        for (var a = 0; a < timestamps.length; a++) {
          var tmpTimestamp = parseInt(timestamps[a])
          if (follows[a].hasOwnProperty('lastRead') == false || follows[a].lastRead == 0) {
              follows[a].lastRead = tmpTimestamp;
          } else if (follows[a].lastRead < tmpTimestamp) {
              newPastes[follows[a].account] = tmpTimestamp;
              bFoundUnread = true;
          }
        }
        localStorage.setItem('following', JSON.stringify(follows));
        events.emit('update');
        resolve();
      })
    })
    .catch((err)=>{
      console.log(err)
    })
  })
}

var getAll = function() {
    var results = [];
    for (var a = 0; a < follows.length; a++) {
        var tmpRow = {
            account: follows[a].account,
            lastRead: follows[a].lastRead
        }
        if (newPastes[follows[a].account] > 0)
            tmpRow.newPaste = newPastes[follows[a].account];
        results.push(tmpRow);
    }
    return results;
}

var add = function(account, lastRead) {
    if (account == null)
        throw new Error("account cant be null");
    for (var a = 0; a < follows.length; a++) {
        if (follows[a].account == account)
            return;
    }
    if (lastRead == null)
        lastRead = 0;
    follows.push({
        account: account,
        lastRead: lastRead
    });
    localStorage.setItem('following', JSON.stringify(follows));
}

var remove = function(account) {
    var bFound = false;
    for (var a = 0; a < follows.length; a++) {
        if (follows[a].account == account) {
            bFound = true;
            follows.splice(a,1)
        }
    }
    if (bFound)
        localStorage.setItem('following', JSON.stringify(follows));

    if (newPastes.hasOwnProperty(account)) {
        delete newPastes[account];
        events.emit('update');
    }
}

var isFollowing = function(account) {
    var bFound = false;
    for (var a = 0; a < follows.length; a++) {
        if (follows[a].account == account) {
            bFound = true;
            break;
        }
    }
    if (bFound)
        return true;
    else
        return false;
}

var getTotalUnread = function() {
    return Object.keys(newPastes).length;
}

var markAsRead = function(account, newTimestamp) {
    var bFound = false;
    for (var a = 0; a < follows.length; a++) {
        if (follows[a].account == account) {
            follows[a].lastRead = newTimestamp;
            bFound = true;
            break;
        }
    }
    if (bFound) {
        localStorage.setItem('following', JSON.stringify(follows));
    }
    if (newPastes.hasOwnProperty(account)) {
        delete newPastes[account];
        events.emit('update');
    }
}

var addNewPaste = function(account, newTimestamp) {
    newPastes[account] = newTimestamp;
    events.emit('update');
}

module.exports = {
    useLocalStorage: useLocalStorage,
    init: init,
    checkAllForUpdates: checkAllForUpdates,
    getAll: getAll,
    add: add,
    remove: remove,
    getTotalUnread: getTotalUnread,
    markAsRead: markAsRead,
    isFollowing: isFollowing,
    addNewPaste: addNewPaste,
    events: events
}


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var BranchSet = __webpack_require__(6);
var Account = __webpack_require__(0);
var Peers = __webpack_require__(1);

var requestInviteStatus = function() {
  return new Promise((resolve, reject)=>{
    Peers.getActivePeerSession()
    .then((session)=>{
      return session.sendMessage({method:"ui_requestInvite",params:["status"]})
    })
    .then((response)=>{
      resolve(response.result);
    })
    .catch((err)=>{
      reject(err);
    })
  })
}

var requestInvite = function(inviteCode) {
  return new Promise((resolve, reject)=>{
    Peers.getActivePeerSession()
    .then((session)=>{
      return session.sendMessage({method:"ui_requestInvite",params:[ inviteCode ]})
    })
    .then((response)=>{
      resolve(response.result);
    })
    .catch((err)=>{
      reject(err);
    })
  })
}


module.exports = {
  requestInviteStatus: requestInviteStatus,
  requestInvite: requestInvite
}


/***/ })
/******/ ]);